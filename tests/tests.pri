INCLUDEPATH += \
    $${QCONNMAN_INCLUDEPATH}
LIBS += -L$${OUT_PWD}/$${DEPTH}/lib $${QCONNMAN_LIBS}
QT = core network testlib dbus
QT -= gui
CONFIG += testcase
CONFIG -= app_bundle
