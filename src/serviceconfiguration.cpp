#include <QSettings>
#include <QFile>
#include <QMetaProperty>
#include <QMap>
#include <QStringList>

#include "qconnman_debug.h"

#include "configurationformat.h"
#include "serviceconfiguration_p.h"
#include "serviceconfiguration.h"

void ConfigurationIPV4Data::apply()
{
    Service *service = qobject_cast<Service*>(parent());
    if (!service) {
        qConnmanDebug() << Q_FUNC_INFO << "invalid parent";
        return;
    }

    QString data;
    if (method() == QLatin1String("off") || method() == QLatin1String("dhcp"))
        data = method();
    else
        data = QString("%1/%2/%3").arg(address()).arg(netmask()).arg(gateway());
    setServiceProperty("IPv4", data);
}

void ConfigurationIPV4Data::loadFromConfigData(const QString &data)
{
    if (data == QLatin1String("off") || data == QLatin1String("dhcp")) {
        setMethod(data);
        setAddress(QString());
        setNetmask(QString());
        setGateway(QString());
        return;
    }

    QStringList parts = data.split("/");
    if (parts.length() < 3) {
        qConnmanDebug() << Q_FUNC_INFO << "invalid data";
        return;
    }

    setAddress(parts.at(0));
    setNetmask(parts.at(1));
    setGateway(parts.at(2));
}

void ConfigurationIPV6Data::apply()
{
    Service *service = qobject_cast<Service*>(parent());
    if (!service) {
        qConnmanDebug() << Q_FUNC_INFO << "invalid parent";
        return;
    }

    QString data;
    if (method() == QLatin1String("off") || method() == QLatin1String("dhcp"))
        data = method();
    else
        data = QString("%1/%2/%3").arg(address()).arg(prefixLength()).arg(gateway());
    setServiceProperty("IPv6", data);
}

void ConfigurationIPV6Data::loadFromConfigData(const QString &data)
{
    if (data == QLatin1String("off") || data == QLatin1String("dhcp")) {
        setMethod(data);
        setAddress(QString());
        setPrefixLength(QString());
        setGateway(QString());
        return;
    }

    QStringList parts = data.split("/");
    if (parts.length() < 3) {
        qConnmanDebug() << Q_FUNC_INFO << "invalid data";
        return;
    }

    setAddress(parts.at(0));
    setPrefixLength(parts.at(1));
    setGateway(parts.at(2));
}

///////////////////////////////////////////////////////////////////////////////////////

bool ConfigurationPrivateInterface::saveConfiguration()
{
    QSettings configurationFile(configurationFilePath, QConnman::ConfigurationFormat);
    configurationFile.beginGroup(QString(QLatin1String("service_%1")).arg(q_ptr->name()));

    // write all configuration data
    foreach (QString key, configurationData.keys())
        configurationFile.setValue(key.replace(".", "/"), configurationData.value(key));

    configurationFile.endGroup();
    configurationFile.sync();
    return (configurationFile.status() == QSettings::NoError);
}

bool ConfigurationPrivateInterface::setConnmanPropertyHelper(const QString &property,
                                                             const QVariant &value)
{
    // convert to a QSettings happy format
    QString modProperty(property);
    if (modProperty.contains(".Configuration"))
        modProperty.remove(".Configuration");

    // this could start getting large, consider moving to a helper
    // or key hash
    if (modProperty == QLatin1String("Domains"))
        modProperty = "SearchDomains";

    configurationData.insert(modProperty.replace(".", "/"), value);
    return true;
}

void ConfigurationPrivateInterface::initializeConfiguredDataObjects(ServicePrivate *d, Service *parent)
{
    // NOTE: this is a little inefficient because it already creates the ipv4/ipv6
    // objects and I'm just deleting them. Look into this, maybe there's no need
    // to call initializeComplexProperties()

    d->ipv4->deleteLater();
    d->ipv4 = new ConfigurationIPV4Data(parent);
    d->ipv4->setObjectName("IPv4");
    d->ipv4Configuration->deleteLater();
    d->ipv4Configuration = new ConfigurationIPV4Data(parent);
    d->ipv4Configuration->setObjectName("IPv4.Configuration");

    d->ipv6->deleteLater();
    d->ipv6 = new ConfigurationIPV6Data(parent);
    d->ipv6->setObjectName("IPv6");
    d->ipv6Configuration->deleteLater();
    d->ipv6Configuration = new ConfigurationIPV6Data(parent);
    d->ipv6Configuration->setObjectName("IPv6.Configuration");
}

void ConfigurationPrivateInterface::loadConfigurationDataFromFile(const QString &path)
{
    if (QFile::exists(path)) {
        QString serviceName = QString("service_%1").arg(q_ptr->name());
        QSettings configurationFile(path, QConnman::ConfigurationFormat);
        configurationFile.beginGroup(serviceName);

        foreach (QString key, configurationFile.childKeys()) {
            QByteArray propertyName = key.toLatin1();
            if (key == QLatin1String("SearchDomains"))
                propertyName = "Domains";

            if (!q_ptr->setProperty(propertyName.data(), configurationFile.value(key))) {
                QVariant member = q_ptr->property(propertyName.data());
                if (member.canConvert<QObject*>()) {
                    QObject *o = member.value<QObject*>();
                    ConfigurableObject *co = qobject_cast<ConfigurableObject*>(o);
                    if (co)
                        co->loadFromConfigData(configurationFile.value(key).toString());
                }
            }
        }
    }
}

void ConfigurationPrivateInterface::loadConfigurationDataFromServiceHelper(QObject *object, QObject *child)
{
    const QMetaObject *mo = object->metaObject();
    for (int i = object->staticMetaObject.propertyOffset(); i < mo->propertyCount(); ++i) {
        QMetaProperty metaProperty = mo->property(i);
        QVariant metaPropertyValue = metaProperty.read(object);
        if (metaPropertyValue.canConvert<QObject*>()) {
            QObject *childObject = metaPropertyValue.value<QObject*>();
            QObject *childChild = child->property(metaProperty.name()).value<QObject*>();
            loadConfigurationDataFromServiceHelper(childObject, childChild);
        } else {
            child->setProperty(metaProperty.name(), metaPropertyValue);
        }
    }
}

void ConfigurationPrivateInterface::loadConfigurationDataFromService(Service *service)
{
    loadConfigurationDataFromServiceHelper(service, q_ptr);
}

bool ServiceConfigurationPrivate::setConnmanProperty(const QString &property, const QVariant &value)
{
    return setConnmanPropertyHelper(property, value);
}

ServiceConfiguration::ServiceConfiguration(const QString &name, const QString &path, QObject *parent)
    : Service(new ServiceConfigurationPrivate(path, this), parent)
{
    Q_D(ServiceConfiguration);
    d->initializeComplexProperties();
    d->initializeConfiguredDataObjects(d, this);

    d->name = name;
    d->loadConfigurationDataFromFile(path);
    if (d->type.isEmpty()) {
        d->setConnmanProperty("Type", "ethernet");
        setProperty("Type", "ethernet");
    }
}

ServiceConfiguration::ServiceConfiguration(Service *service, const QString &path, QObject *parent)
    : Service(new ServiceConfigurationPrivate(path, this), parent)
{
    Q_D(ServiceConfiguration);
    d->initializeComplexProperties();
    d->initializeConfiguredDataObjects(d, this);

    if (!service) {
        qConnmanDebug() << Q_FUNC_INFO << "invalid service";
        return;
    }

    d->name = service->name();
    d->loadConfigurationDataFromService(service);
    d->loadConfigurationDataFromFile(path);
    if (d->type.isEmpty()) {
        d->setConnmanProperty("Type", "ethernet");
        setProperty("Type", "ethernet");
    }
}

ServiceConfiguration::~ServiceConfiguration()
{
}

QString ServiceConfiguration::macAddress() const
{
    Q_D(const ServiceConfiguration);
    return d->macAddress;
}

void ServiceConfiguration::setMacAddress(const QString &macAddress)
{
    Q_D(ServiceConfiguration);
    d->setConnmanProperty("MAC", macAddress);
}

void ServiceConfiguration::setMacAddressInternal(const QString &macAddress)
{
    Q_D(ServiceConfiguration);
    d->macAddress = macAddress;
}

QString ServiceConfiguration::domain() const
{
    Q_D(const ServiceConfiguration);
    return d->domain;
}

void ServiceConfiguration::setDomain(const QString &domain)
{
    Q_D(ServiceConfiguration);
    d->setConnmanProperty("Domain", domain);
}

void ServiceConfiguration::setDomainInternal(const QString &domain)
{
    Q_D(ServiceConfiguration);
    d->domain = domain;
}

bool ServiceConfiguration::save()
{
    Q_D(ServiceConfiguration);
    return d->saveConfiguration();
}

/////////////////////////////////////////////////////////////////////////////

bool WifiServiceConfigurationPrivate::setConnmanProperty(const QString &property, const QVariant &value)
{
    return setConnmanPropertyHelper(property, value);
}

WifiServiceConfiguration::WifiServiceConfiguration(const QString &name,
                                                   const QString &path, QObject *parent)
    : WifiService(new WifiServiceConfigurationPrivate(path, this), parent)
{
    Q_D(WifiServiceConfiguration);
    d->initializeComplexProperties();
    d->initializeConfiguredDataObjects(d, this);

    d->name = name;
    d->loadConfigurationDataFromFile(path);
    if (d->type.isEmpty()) {
        d->setConnmanProperty("Type", "wifi");
        setProperty("Type", "wifi");
    }
}

WifiServiceConfiguration::WifiServiceConfiguration(Service *service, const QString &path, QObject *parent)
    : WifiService(new WifiServiceConfigurationPrivate(path, this), parent)
{
    Q_D(WifiServiceConfiguration);
    d->initializeComplexProperties();
    d->initializeConfiguredDataObjects(d, this);

    if (!service) {
        qConnmanDebug() << Q_FUNC_INFO << "invalid service";
        return;
    }

    d->name = service->name();
    d->loadConfigurationDataFromService(service);
    d->loadConfigurationDataFromFile(path);
    if (d->type.isEmpty()) {
        d->setConnmanProperty("Type", "wifi");
        setProperty("Type", "wifi");
    }
}


WifiServiceConfiguration::~WifiServiceConfiguration()
{
}

QString WifiServiceConfiguration::macAddress() const
{
    Q_D(const WifiServiceConfiguration);
    return d->macAddress;
}

void WifiServiceConfiguration::setMacAddress(const QString &macAddress)
{
    Q_D(WifiServiceConfiguration);
    d->setConnmanProperty("MAC", macAddress);
}

void WifiServiceConfiguration::setMacAddressInternal(const QString &macAddress)
{
    Q_D(WifiServiceConfiguration);
    d->macAddress = macAddress;
}

QString WifiServiceConfiguration::name() const
{
    Q_D(const WifiServiceConfiguration);
    return d->name;
}

void WifiServiceConfiguration::setName(const QString &name)
{
    Q_D(WifiServiceConfiguration);
    d->setConnmanProperty("Name", name);
}

void WifiServiceConfiguration::setNameInternal(const QString &name)
{
    Q_D(WifiServiceConfiguration);
    d->name = name;
}

bool WifiServiceConfiguration::save()
{
    Q_D(WifiServiceConfiguration);
    return d->saveConfiguration();
}
