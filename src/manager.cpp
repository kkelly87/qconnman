/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QVariantMap>
#include <QIcon>

#include "qconnman_debug.h"

#include "interfaces/manager_interface.h"

#include "configurationformat.h"
#include "manager_p.h"
#include "manager.h"

bool isDebugging = true;
ManagerNode::ManagerNode()
    : m_parent(0),
      m_technology(false)
{
}

ManagerNode::ManagerNode(Technology *technology, ManagerNode *parent)
    : m_parent(parent),
      m_object(technology),
      m_technology(true)
{
}

ManagerNode::ManagerNode(Service *service, ManagerNode *parent)
    : m_parent(parent),
      m_object(service),
      m_technology(false)
{
}

ManagerNode::~ManagerNode()
{
    qDeleteAll(m_children);
}

bool ManagerNode::isTechnology() const
{
    return (!m_object.isNull() && m_technology == true);
}

bool ManagerNode::isService() const
{
    return (!m_object.isNull() && m_technology == false);
}

bool ManagerNode::isRoot() const
{
    return m_object.isNull();
}

QDBusObjectPath ManagerNode::path() const
{
    if (isService()) {
        Service *service = qobject_cast<Service*>(m_object);
        return service->objectPath();
    } else if (isTechnology()) {
        Technology *technology = qobject_cast<Technology*>(m_object);
        return technology->path();
    }

    return QDBusObjectPath();
}

ManagerNode *ManagerNode::parent()
{
    return m_parent;
}

ManagerNode *ManagerNode::child(int index)
{
    if (index >= m_children.size())
        return 0;
    return m_children.at(index);
}

void ManagerNode::appendChild(ManagerNode *node)
{
    node->m_parent = this;
    m_children.append(node);
}

int ManagerNode::childCount() const
{
    return m_children.size();
}

int ManagerNode::childNumber() const
{
    if (m_parent)
        return m_parent->m_children.indexOf(const_cast<ManagerNode*>(this));
    return 0;
}

bool ManagerNode::removeChildren(int position, int count)
{
    if (position < 0 || position + count > m_children.size())
        return false;

    for (int row = 0; row < count; ++row)
        delete m_children.takeAt(position);
    return true;
}

QHash<int, QByteArray> ManagerPrivate::s_roleNames;
QHash<QString, Manager::State> ManagerPrivate::s_stateLookup;
int ManagerPrivate::s_objectPropertyDataMetaTypeId = qDBusRegisterMetaType<ObjectPropertyData>();
int ManagerPrivate::s_objectPropertyListDataMetaTypeId = qDBusRegisterMetaType<QList<ObjectPropertyData> >();
int ManagerPrivate::s_serviceMetaTypeId = qRegisterMetaType<Service*>("Service*");
int ManagerPrivate::s_technologyMetaTypeId = qRegisterMetaType<Technology*>("Technology*");

Manager::Manager(bool initialize, QObject *parent)
    : QAbstractItemModel(parent),
      d_ptr(new ManagerPrivate)
{
    Q_UNUSED(initialize)
}

Manager::Manager(QObject *parent)
    : QAbstractItemModel(parent),
      d_ptr(new ManagerPrivate)
{
    initialize();
}

void Manager::connmanRegistered()
{
    Q_D(Manager);
    qConnmanDebug() << Q_FUNC_INFO;
    if (d->managerInterface)
        d->managerInterface->deleteLater();

    d->managerInterface =
        new NetConnmanManagerInterface("net.connman", "/", QDBusConnection::systemBus(), this);

    if (!d->managerInterface->isValid()) {
        qConnmanDebug() << "manager interface is invalid, aborting...";
        return;
    }

    connect(d->managerInterface, SIGNAL(PropertyChanged(QString,QDBusVariant)),
                          this, SLOT(propertyChanged(QString,QDBusVariant)));
    connect(d->managerInterface, SIGNAL(ServicesChanged(QList<ObjectPropertyData>,QList<QDBusObjectPath>)),
                          this, SLOT(servicesChanged(QList<ObjectPropertyData>,QList<QDBusObjectPath>)));

    connect(d->managerInterface, SIGNAL(TechnologyAdded(QDBusObjectPath,QVariantMap)),
                          this, SLOT(technologyAdded(QDBusObjectPath,QVariantMap)));
    connect(d->managerInterface, SIGNAL(TechnologyRemoved(QDBusObjectPath)),
                          this, SLOT(technologyRemoved(QDBusObjectPath)));

    // technologies
    QDBusPendingReply<QList<ObjectPropertyData> > tReply = d->managerInterface->GetTechnologies();
    QDBusPendingCallWatcher *tWatcher = new QDBusPendingCallWatcher(tReply, this);
    connect(tWatcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                this, SLOT(getTechnologiesResponse(QDBusPendingCallWatcher*)));

    // services
    QDBusPendingReply<QList<ObjectPropertyData> > sReply = d->managerInterface->GetServices();
    QDBusPendingCallWatcher *sWatcher = new QDBusPendingCallWatcher(sReply, this);
    connect(sWatcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                this, SLOT(getServicesResponse(QDBusPendingCallWatcher*)));

    // properties
    QDBusPendingReply<QVariantMap> pReply = d->managerInterface->GetProperties();
    QDBusPendingCallWatcher *pWatcher = new QDBusPendingCallWatcher(pReply, this);
    connect(pWatcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                this, SLOT(getPropertiesResponse(QDBusPendingCallWatcher*)));


    // block (for command line use)
    tWatcher->waitForFinished();
    sWatcher->waitForFinished();
    pWatcher->waitForFinished();
}

void Manager::connmanUnregistered()
{
    Q_D(Manager);
    qConnmanDebug() << Q_FUNC_INFO;
    if (d->managerInterface) {
        d->managerInterface->deleteLater();
        d->managerInterface = 0;
    }
}

Manager::~Manager()
{
}

bool Manager::hasService(const QDBusObjectPath &path) const
{
    Q_D(const Manager);
    return d->services.contains(path);
}

Service *Manager::service(const QDBusObjectPath &path)
{
    Q_D(Manager);
    return d->services.value(path);
}

Service *Manager::service(const QString &name)
{
    Q_D(Manager);
    foreach (Service *service, d->services.values()) {
        if (service && service->name() == name)
            return service;
    }

    return 0;
}

Service *Manager::connectedService()
{
    Q_D(Manager);
    foreach (Service *service, d->services.values()) {
        if (service && service->state() == Service::OnlineState)
            return service;
    }

    return 0;
}

void Manager::disconnectServices()
{
    Q_D(const Manager);
    foreach (Service *service, d->services.values()) {
        if (service && service->state() != Service::DisconnectState)
            service->disconnect();
    }
}

bool Manager::hasService(const QString &name) const
{
    Q_D(const Manager);
    foreach (Service *service, d->services.values()) {
        if (service && service->name() == name)
            return true;
    }

    return false;
}

QStringList Manager::configuredServices(const QString &path)
{
    QString configurationPath =
        ((path.isEmpty() || path.isNull()) ? QLatin1String("/var/lib/connman/qconnman.config") : path);
    QSettings configuration(configurationPath, QConnman::ConfigurationFormat);

    QStringList result;
    foreach (QString group, configuration.childGroups()) {
        if (group.startsWith("service_"))
            result << group.remove("service_");
    }

    return result;
}

QList<Agent*> Manager::agents() const
{
    Q_D(const Manager);
    return d->agents.values();
}

QList<Technology*> Manager::technologies() const
{
    Q_D(const Manager);
    return d->technologies;
}

QList<Service*> Manager::services() const
{
    Q_D(const Manager);
    return d->services.values();
}

void Manager::registerAgent(Agent *agent)
{
    Q_D(Manager);
    QString agentPath = agent->path().path();
    if (agentPath.isEmpty() || agentPath.isNull()) {
        qConnmanDebug() << "invalid agent path, aborting...";
        return;
    }

    d->agents.insert(agent->path(), agent);
    QDBusConnection::systemBus().registerObject(agentPath, agent);
    d->managerInterface->RegisterAgent(agent->path());
}

void Manager::unregisterAgent(Agent *agent)
{
    unregisterAgent(agent->path());
}

void Manager::unregisterAgent(const QDBusObjectPath &path)
{
    Q_D(Manager);
    if (!d->agents.contains(path)) {
        qConnmanDebug() << "agent(" << path.path() << ") does not exist, aborting...";
        return;
    }

    d->managerInterface->UnregisterAgent(path);
    QDBusConnection::systemBus().unregisterObject(path.path());
    Agent *agent = d->agents.take(path);
    agent->deleteLater();
}

Manager::State Manager::state() const
{
    Q_D(const Manager);
    return d->state;
}

void Manager::setStateInternal(const State state)
{
    Q_D(Manager);
    d->state = state;
    Q_EMIT stateChanged();
}

bool Manager::offlineMode() const
{
    Q_D(const Manager);
    return d->offlineMode;
}

void Manager::setOfflineMode(bool offlineMode)
{
    Q_UNUSED(offlineMode)
    // send stuff to connman!
}

void Manager::setOfflineModeInternal(bool offlineMode)
{
    Q_D(Manager);
    d->offlineMode = offlineMode;
    Q_EMIT offlineModeChanged();
}

bool Manager::sessionMode() const
{
    Q_D(const Manager);
    return d->sessionMode;
}

void Manager::setSessionMode(bool sessionMode)
{
    Q_UNUSED(sessionMode)
    // send stuff to connman!
}

void Manager::setState(State state)
{
    Q_UNUSED(state)
    // send stuff to connman!
}

void Manager::setSessionModeInternal(bool sessionMode)
{
    Q_D(Manager);
    d->sessionMode = sessionMode;
    Q_EMIT sessionModeChanged();
}


QDBusObjectPath Manager::connectProvider(const QVariant &provider)
{
    Q_D(Manager);
    if (!provider.canConvert<QVariantMap>()) {
        qConnmanDebug() << Q_FUNC_INFO << "invalid provider";
        return QDBusObjectPath();
    }

    QVariantMap providerData = provider.value<QVariantMap>();
    QDBusPendingReply<QDBusObjectPath> reply = d->managerInterface->ConnectProvider(providerData);
    reply.waitForFinished();
    if (reply.isError() || !reply.isValid()) {
        qConnmanDebug() << Q_FUNC_INFO << "error: " << reply.error();
        return QDBusObjectPath();
    }

    d->providers.insert(reply.value(), providerData.value("Name").toString());
    return reply.value();
}

bool Manager::removeProvider(const QDBusObjectPath &path)
{
    Q_D(Manager);
    if (!d->providers.contains(path))
        return false;

    d->providers.remove(path);
    QDBusPendingReply<> reply = d->managerInterface->RemoveProvider(path);
    reply.waitForFinished();
    if (reply.isError() || !reply.isValid()) {
        qConnmanDebug() << Q_FUNC_INFO << "error: " << reply.error();
        return false;
    }

    return true;
}

bool Manager::removeProvider(const QString &provider)
{
    Q_D(Manager);
    if (!d->providers.values().contains(provider))
        return false;

    QDBusObjectPath path = d->providers.key(provider);
    return removeProvider(path);
}

QString Manager::provider(const QDBusObjectPath &path) const
{
    Q_D(const Manager);
    return d->providers.value(path);
}

QDBusObjectPath Manager::provider(const QString &provider)
{
    Q_D(const Manager);
    return d->providers.key(provider);
}

QList<QString> Manager::providers() const
{
    Q_D(const Manager);
    return d->providers.values();
}

QStringList Manager::ignoredTechnologies() const
{
    return QStringList();
}

bool Manager::ignoreHiddenNetworks() const
{
    Q_D(const Manager);
    return d->ignoreHiddenNetworks;
}

void Manager::setIgnoreHiddenNetworks(bool hide)
{
    Q_D(Manager);
    if ( hide != d->ignoreHiddenNetworks ) {
        d->ignoreHiddenNetworks = hide;
        Q_EMIT ignoreHiddenNetworksChanged();
    }
}

QString Manager::technologyName(Technology *technology) const
{
    return technology->name();
}

QIcon Manager::technologyIcon(Technology *technology) const
{
    if (technology->type() == "ethernet")
        return QIcon::fromTheme("network-wired");
    if (technology->type() == "wifi")
        return QIcon::fromTheme("network-wireless");
    return QIcon();
}

QString Manager::serviceName(Service *service) const
{
    QString name = service->name();
    if (!name.isEmpty())
        return name;
    else
        return QString("Hidden network (%1)").arg(service->security().join(" ").toUpper());
}

QIcon Manager::serviceIcon(Service *service) const
{
    Q_UNUSED(service)
    return QIcon();
}

QString Manager::iconName(const QString &type) const
{
    if (type == QLatin1String("ethernet"))
        return "network-wired";
    if (type == QLatin1String("wifi"))
        return "network-wireless";
    return QString();
}

void Manager::initialize()
{
    Q_D(Manager);
    qConnmanDebug() << Q_FUNC_INFO;

    // not sure if there is a better place for these guys
    static const int debugging = qgetenv("QCONNMAN_DEBUG").toInt();
    ::isDebugging = debugging;

    d->root = new ManagerNode;
    d->connmanWatcher = new QDBusServiceWatcher("net.connman",
                                               QDBusConnection::systemBus(),
                                               QDBusServiceWatcher::WatchForRegistration | QDBusServiceWatcher::WatchForUnregistration,
                                               this);
    connect(d->connmanWatcher, SIGNAL(serviceRegistered(QString)), this, SLOT(connmanRegistered()));
    connect(d->connmanWatcher, SIGNAL(serviceUnregistered(QString)), this, SLOT(connmanUnregistered()));

    // we could be starting after connman, so fake the first one
    connmanRegistered();
}

void Manager::propertyChanged(const QString &name, const QDBusVariant &value)
{
    setObjectProperty(this, name, value.variant());
}

void Manager::setObjectProperty(QObject *object, const QString &property, const QVariant &value)
{
    QVariant fixedValue;
    int idx = object->metaObject()->indexOfProperty(property.toLatin1());
    if (idx == -1)
        return;

    if (property == "State")
        fixedValue = ManagerPrivate::s_stateLookup.value(value.toString());
    else
        fixedValue = value;

    if (!object->metaObject()->property(idx).write(object, fixedValue)) {
        qConnmanDebug() << Q_FUNC_INFO << "could not write property: " << property;
    }
}

void Manager::getTechnologiesResponse(QDBusPendingCallWatcher *call)
{
    QDBusPendingReply<QList<ObjectPropertyData> > reply = *call;
    if (reply.isError()) {
        qConnmanDebug() << Q_FUNC_INFO << "error: " << reply.error().message();
    } else {
        QList<ObjectPropertyData> result = reply.value();
        foreach (ObjectPropertyData data, result)
            technologyAdded(data.path, data.properties);
    }

    call->deleteLater();
}

void Manager::getServicesResponse(QDBusPendingCallWatcher *call)
{
    QDBusPendingReply<QList<ObjectPropertyData> > reply = *call;
    if (reply.isError()) {
        qConnmanDebug() << Q_FUNC_INFO << "error: " << reply.error().message();
    } else {
        QList<ObjectPropertyData> result = reply.value();
        servicesChanged(result, QList<QDBusObjectPath>());
    }

    call->deleteLater();
}

void Manager::getPropertiesResponse(QDBusPendingCallWatcher *call)
{
    QDBusPendingReply<QVariantMap> reply = *call;
    if (reply.isError()) {
        qConnmanDebug() << Q_FUNC_INFO << "error: " << reply.error().message();
    } else {
        QVariantMap result = reply.value();
        foreach (QString property, result.keys())
            setObjectProperty(this, property, result.value(property));
    }

    call->deleteLater();
}

void Manager::servicesChanged(const QList<ObjectPropertyData> &changedServices,
                              const QList<QDBusObjectPath> &removedServices)
{
    Q_D(Manager);
    foreach (ObjectPropertyData data, changedServices) {
      if (d->nodeForPath(data.path, d->root)) {
            qConnmanDebug() << "\tchanged service(" << data.path.path() << ")";
        } else {
            QString technologyType = data.properties.value("Type").toString();
            ManagerNode *parentNode = d->nodeForTechnologyType(technologyType);
            if (!parentNode) {
                qConnmanDebug() << "\tinvalid technology specified: " << technologyType << ", trying to add...";
                technologyAdded(data.path, data.properties);
                parentNode = d->nodeForTechnologyType(technologyType);
            }

            if (parentNode) {
                Service *service = new Service(data, this);
                connect( service, SIGNAL(connected()), this, SIGNAL(connectedServiceChanged()));
                if (service->type() == "wifi" && service->name().isEmpty() && ignoreHiddenNetworks()) {
                    d->services[service->objectPath()] = service;
                    continue;
                }
                QModelIndex parent = index(parentNode->childNumber(), 0, QModelIndex());
                beginInsertRows(parent, parentNode->childCount(), parentNode->childCount() + 1);
                parentNode->appendChild(new ManagerNode(service));
                d->services[service->objectPath()] = service;
                endInsertRows();
                qConnmanDebug() << "\tadded service(" << data.path.path() << ")";
            } else {
                qConnmanDebug() << "\tinvalid technology specified: " << technologyType << ", properties: " << data.properties;
            }
        }
    }

    foreach (QDBusObjectPath path, removedServices) {
        ManagerNode *serviceNode = d->nodeForPath(path, d->root);
        if (!serviceNode) {
            if (d->services.contains(path)) {
                Service *service = d->services[path];
                service->deleteLater();
                d->services.remove(path);
                qDebug() << "service destroyed" << path.path();
            }
            continue;
        }

        ManagerNode *technologyNode = serviceNode->parent();
        QModelIndex parent = index(technologyNode->childNumber(), 0, QModelIndex());
        beginRemoveRows(parent, serviceNode->childNumber(), serviceNode->childNumber());
        Service *service = serviceNode->object<Service*>();
        service->deleteLater();
        technologyNode->removeChildren(serviceNode->childNumber(), 1);
        endRemoveRows();
        d->services.remove(path);
        qConnmanDebug() << "\tremoved service(" << path.path() << ")";
    }

    Q_EMIT servicesChanged();
}

void Manager::technologyAdded(const QDBusObjectPath &path, const QVariantMap &properties)
{
    Q_D(Manager);
    ManagerNode *node = d->nodeForPath(path, d->root);
    if (node) {
        qConnmanDebug() << "attempted addition of known technology(" << path.path() << "), aborting...";
        return;
    }

    Technology *technology = new Technology(path, properties, this);
    if (ignoredTechnologies().contains(technology->type())) {
        technology->deleteLater();
        return;
    }

    beginInsertRows(QModelIndex(), d->root->childCount(), d->root->childCount() + 1);
    d->root->appendChild(new ManagerNode(technology));
    endInsertRows();

    d->technologies.append(technology);
    qConnmanDebug() << "added technology(" << path.path() << ")";
}

void Manager::technologyRemoved(const QDBusObjectPath &path)
{
    Q_D(Manager);
    ManagerNode *node = d->nodeForPath(path, d->root);
    if (!node) {
        qConnmanDebug() << "attempted removal of unknown technology(" << path.path() << "), aborting...";
        return;
    }

    if (!node->isTechnology()) {
        qConnmanDebug() << "attempted removal of node which is not a technology, aborting...";
        return;
    }

    beginRemoveRows(QModelIndex(), node->childNumber(), node->childNumber());
    Technology *technology = node->object<Technology*>();
    technology->deleteLater();
    d->root->removeChildren(node->childNumber(), 1);
    endRemoveRows();
    d->technologies.removeAll(technology);
    qConnmanDebug() << "removed technology(" << path.path() << ")";
}

ManagerNode *ManagerPrivate::nodeForIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        ManagerNode *node = static_cast<ManagerNode*>(index.internalPointer());
        if (node)
            return node;
    }

    return root;
}

ManagerNode *ManagerPrivate::nodeForPath(const QDBusObjectPath &path, ManagerNode *parent) const
{
    for (int i = 0; i < parent->childCount(); i++) {
        ManagerNode *child = parent->child(i);
        if (child->path() == path)
            return child;

        ManagerNode *result = nodeForPath(path, child);
        if (result)
            return result;
    }

    return 0;
}

ManagerNode *ManagerPrivate::nodeForTechnologyType(const QString &type)
{
    for (int i = 0; i < root->childCount(); i++) {
        ManagerNode *node = root->child(i);
        Technology *technology = node->object<Technology*>();
        if (technology->type() == type)
            return node;
    }

    return 0;
}

QHash<int, QByteArray> Manager::roleNames() const
{
    return ManagerPrivate::s_roleNames;
}

QVariant Manager::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    ManagerNode *node = static_cast<ManagerNode*>(index.internalPointer());
    if (role == Qt::DisplayRole) {
        if ( node->isTechnology() )
            return technologyName(node->object<Technology*>());
        if ( node->isService() )
            return serviceName(node->object<Service*>());
        return node->path().path();
    } else if (role == Qt::DecorationRole) {
        if (node->isTechnology())
            return QIcon::fromTheme(iconName(node->object<Technology*>()->type()));
        else if (node->isService())
            return QIcon::fromTheme(iconName(node->object<Service*>()->type()));
    } else if (role == IconRole) {
        if (node->isTechnology())
            return iconName(node->object<Technology*>()->type());
        else if (node->isService())
            return iconName(node->object<Service*>()->type());
    } else if (role == ServiceRole) {
        if (node->isService())
            return qVariantFromValue(node->object<Service*>());
    } else if (role == TechnologyRole) {
        if (node->isTechnology())
            return qVariantFromValue(node->object<Technology*>());
    }

    return QVariant();
}

QModelIndex Manager::index(int row, int column, const QModelIndex &parent) const
{
    Q_D(const Manager);
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    ManagerNode *parentNode = d->nodeForIndex(parent);
    ManagerNode *childNode = parentNode->child(row);
    if (childNode)
        return createIndex(row, column, childNode);
    return QModelIndex();

}

QModelIndex Manager::parent(const QModelIndex &index) const
{
    Q_D(const Manager);
    if (!index.isValid())
        return QModelIndex();

    ManagerNode *childNode = d->nodeForIndex(index);
    ManagerNode *parentNode = childNode->parent();
    if (parentNode == d->root)
        return QModelIndex();
    return createIndex(parentNode->childNumber(), 0, parentNode);
}

int Manager::rowCount(const QModelIndex &parent) const
{
    Q_D(const Manager);
    ManagerNode *parentNode = d->nodeForIndex(parent);
    return parentNode->childCount();
}

int Manager::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 1;
}
