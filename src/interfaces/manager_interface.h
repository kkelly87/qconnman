/*
 * This file was generated by qdbusxml2cpp version 0.8
 * Command line was: qdbusxml2cpp -i types.h -p manager_interface ../dbus/net.connman.Manager.xml
 *
 * qdbusxml2cpp is Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#ifndef MANAGER_INTERFACE_H_1395075826
#define MANAGER_INTERFACE_H_1395075826

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>
#include "types.h"

/*
 * Proxy class for interface net.connman.Manager
 */
class NetConnmanManagerInterface: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "net.connman.Manager"; }

public:
    NetConnmanManagerInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = 0);

    ~NetConnmanManagerInterface();

public Q_SLOTS: // METHODS
    inline Q_DECL_DEPRECATED QDBusPendingReply<QDBusObjectPath> ConnectProvider(const QVariantMap &provider)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(provider);
        return asyncCallWithArgumentList(QLatin1String("ConnectProvider"), argumentList);
    }

    inline QDBusPendingReply<QDBusObjectPath> CreateSession(const QVariantMap &settings, const QDBusObjectPath &notifier)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(settings) << QVariant::fromValue(notifier);
        return asyncCallWithArgumentList(QLatin1String("CreateSession"), argumentList);
    }

    inline QDBusPendingReply<> DestroySession(const QDBusObjectPath &session)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(session);
        return asyncCallWithArgumentList(QLatin1String("DestroySession"), argumentList);
    }

    inline QDBusPendingReply<QVariantMap> GetProperties()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("GetProperties"), argumentList);
    }

    inline QDBusPendingReply<QList<ObjectPropertyData> > GetServices()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("GetServices"), argumentList);
    }

    inline QDBusPendingReply<QList<ObjectPropertyData> > GetTechnologies()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("GetTechnologies"), argumentList);
    }

    inline QDBusPendingReply<> RegisterAgent(const QDBusObjectPath &path)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(path);
        return asyncCallWithArgumentList(QLatin1String("RegisterAgent"), argumentList);
    }

    inline QDBusPendingReply<> RegisterCounter(const QDBusObjectPath &path, uint accuracy, uint period)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(path) << QVariant::fromValue(accuracy) << QVariant::fromValue(period);
        return asyncCallWithArgumentList(QLatin1String("RegisterCounter"), argumentList);
    }

    inline QDBusPendingReply<> ReleasePrivateNetwork(const QDBusObjectPath &path)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(path);
        return asyncCallWithArgumentList(QLatin1String("ReleasePrivateNetwork"), argumentList);
    }

    inline Q_DECL_DEPRECATED QDBusPendingReply<> RemoveProvider(const QDBusObjectPath &provider)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(provider);
        return asyncCallWithArgumentList(QLatin1String("RemoveProvider"), argumentList);
    }

    inline QDBusPendingReply<QDBusObjectPath, QVariantMap, QDBusUnixFileDescriptor> RequestPrivateNetwork()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("RequestPrivateNetwork"), argumentList);
    }
    inline QDBusReply<QDBusObjectPath> RequestPrivateNetwork(QVariantMap &settings, QDBusUnixFileDescriptor &socket)
    {
        QList<QVariant> argumentList;
        QDBusMessage reply = callWithArgumentList(QDBus::Block, QLatin1String("RequestPrivateNetwork"), argumentList);
        if (reply.type() == QDBusMessage::ReplyMessage && reply.arguments().count() == 3) {
            settings = qdbus_cast<QVariantMap>(reply.arguments().at(1));
            socket = qdbus_cast<QDBusUnixFileDescriptor>(reply.arguments().at(2));
        }
        return reply;
    }

    inline QDBusPendingReply<> SetProperty(const QString &name, const QDBusVariant &value)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(name) << QVariant::fromValue(value);
        return asyncCallWithArgumentList(QLatin1String("SetProperty"), argumentList);
    }

    inline QDBusPendingReply<> UnregisterAgent(const QDBusObjectPath &path)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(path);
        return asyncCallWithArgumentList(QLatin1String("UnregisterAgent"), argumentList);
    }

    inline QDBusPendingReply<> UnregisterCounter(const QDBusObjectPath &path)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(path);
        return asyncCallWithArgumentList(QLatin1String("UnregisterCounter"), argumentList);
    }

Q_SIGNALS: // SIGNALS
    void PropertyChanged(const QString &name, const QDBusVariant &value);
    void ServicesChanged(const QList<ObjectPropertyData> &changed, const QList<QDBusObjectPath> &removed);
    void TechnologyAdded(const QDBusObjectPath &path, const QVariantMap &properties);
    void TechnologyRemoved(const QDBusObjectPath &path);
};

namespace net {
  namespace connman {
    typedef ::NetConnmanManagerInterface Manager;
  }
}
#endif
