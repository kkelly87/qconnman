/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef AGENT_H
#define AGENT_H

#include <QObject>
#include <QDBusObjectPath>
#include <QVariantMap>
#include <QStringList>
#include <QDBusContext>

class Manager;
class Service;
class AgentAdaptor;
class QRegExpValidator;
class Agent: public QObject, protected QDBusContext
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "net.connman.Agent")
    Q_PROPERTY(QDBusObjectPath path READ path)

public:
    struct InputRequest
    {
        struct Response
        {
            QString identity;
            QString passphrase;
            QString name;
        };

        QDBusObjectPath service;
        Response response;
        bool cancel;

        InputRequest(): cancel(false) { }
    };

    struct BrowserRequest
    {
        QDBusObjectPath service;
        QString url;
        bool cancel;
        BrowserRequest(): cancel(false) { }
    };

    struct ErrorRequest
    {
        QDBusObjectPath service;
        QString error;
        bool retry;

        ErrorRequest(): retry(false) { }
    };

    explicit Agent(const QString &path, Manager *parent);
    explicit Agent(const QDBusObjectPath &path, Manager *parent);
    virtual ~Agent();

public:
    QDBusObjectPath path() const;
    Manager *manager() const;

    inline InputRequest *currentInputRequest() const { return m_inputRequest; }
    inline BrowserRequest *currentBrowserRequest() const { return m_browserRequest; }
    inline ErrorRequest *currentErrorRequest() const { return m_errorRequest; }

Q_SIGNALS:
    void identityRequested();
    void passphraseRequested();
    void nameRequested();
    void browserRequested();
    void errorRaised();

private Q_SLOTS:
    friend class AgentAdaptor;

    virtual void Release();
    virtual void ReportError(const QDBusObjectPath &service, const QString &error);
    virtual void RequestBrowser(const QDBusObjectPath &service, const QString &url);
    virtual QVariantMap RequestInput(const QDBusObjectPath &service, const QVariantMap &fields);

private:
    Manager *m_manager;
    AgentAdaptor *m_adaptor;
    QDBusObjectPath m_path;

    InputRequest *m_inputRequest;
    BrowserRequest *m_browserRequest;
    ErrorRequest *m_errorRequest;
};

#endif
