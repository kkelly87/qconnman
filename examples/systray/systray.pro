DEPTH = ../..
include($${DEPTH}/qconnman.pri)

TEMPLATE = app
TARGET = qconnman_systray
DEPENDPATH += .
INCLUDEPATH += $${QCONNMAN_INCLUDEPATH}
LIBS += -L../../lib $${QCONNMAN_LIBS}
QT += declarative

HEADERS += technologywidget.h \
           servicewidget.h \
           managermenuaction.h \
           trayiconcontroller.h
SOURCES += technologywidget.cpp \
           servicewidget.cpp \
           managermenuaction.cpp \
           trayiconcontroller.cpp \
           main.cpp
FORMS += technologywidget.ui \
         servicewidget.ui

RESOURCES += \
    images.qrc

