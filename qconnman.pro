TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS += src \
           tests \
           examples

equals(QT_MAJOR_VERSION, 5): SUBDIRS += quick
