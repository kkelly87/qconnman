#include <QtQml>
#include "manager.h"

class QConnmanQmlPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")
public:
    virtual void registerTypes(const char *uri) {
        Q_ASSERT(QLatin1String(uri) == QLatin1String("DevonIT.QConnman"));

        // @uri DevonIT.QConnman
        qmlRegisterType<Manager>(uri, 1, 0, "Manager");
        qmlRegisterUncreatableType<Service>(uri, 1, 0, "Service", "Service can only be accessed via Manager");
        qmlRegisterUncreatableType<Technology>(uri, 1, 0, "Technology", "Technology can only be accessed via Manager");
    }
};

#include "plugin.moc"
